#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "Driver_USART.h"
#include <stdio.h>
#include <string.h>
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"

#define PUERTO_UART0 0
#define Tx0          2
#define Rx0          3

void UART_init(void);

extern ARM_DRIVER_USART Driver_USART0;
static ARM_DRIVER_USART *USARTdrv = &Driver_USART0;

void myUSART_callback(uint32_t event);
