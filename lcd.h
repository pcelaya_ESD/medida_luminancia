#include "LPC17xx.h"												
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "Driver_SPI.h"
#include "SPI_LPC17xx.h"
#include "stdio.h"

#define PORT 0
#define RESET 8  //DIP6
#define A0 6   //DIP8
#define CS 18  //DIP 11

void retardo(uint32_t n_microsegundos);
void init_lcd(void);
void wr_data(unsigned char data);
void wr_cmd(unsigned char cmd);
void LCD_reset(void);
void copy_to_lcd(void);
int EscribeLetra_L1(uint8_t letra);
int EscribeLetra_L2(uint8_t letra);
void normal_lcd(uint8_t hora, uint8_t min, uint8_t seg,float temp,float TempGuarda, float TempRef);
void lcd_start (void);
void test_izquierda(void);
void test_derecha(void);
void test_abajo(void);
void desarmado_lcd(uint8_t hora, uint8_t min, uint8_t seg,float TempGuarda, float TempRef);
void borrarLCD(void);
void luxLCD(float lux);
