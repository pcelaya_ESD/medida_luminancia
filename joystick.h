#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "stdio.h"
#include "string.h"
#include "lcd.h"
#include "Driver_SPI.h"
#include "SPI_LPC17xx.h"

#define   PUERTO_JOYSTICK      0  
#define   sw_up                23  
#define   sw_down              17  
#define   sw_left              15  
#define   sw_right             24  
#define   sw_center            16 

//#define arriba 1
//#define abajo 2
//#define derecha 3
//#define izquierda 4
//#define centro 5

bool arriba;
bool abajo;
bool izquierda;
bool derecha;
bool centro;

void debounceR (void const *argument);     
void debounceF (void const *argument); 
int Init_joystick (void) ;
