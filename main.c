/*----------------------------------------------------------------------------
 * CMSIS-RTOS 'main' function template
 *---------------------------------------------------------------------------*/

#define osObjectsPublic                     // define objects in main module
#include "osObjects.h"                      // RTOS object definitions
#include "lcd.h"

extern void init_lcd(void);
extern int Init_I2C(void);
extern float getLum(void);
extern int Init_automata (void) ;
extern float lux;
/*
 * main: initialize and start the system
 */
int main (void) {
  osKernelInitialize ();                    // initialize CMSIS-RTOS
	
		Init_I2C();
		Init_automata ();
		osKernelStart ();                         // start thread execution 

}
