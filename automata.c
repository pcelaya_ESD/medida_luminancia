
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "LPC17xx.h"												
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "lcd.h"
#include "usart.h"

#define PUERTO_RGB 2
#define RED 3
#define GREEN 2
#define BLUE 1

typedef enum 
{ 
 INACTIVO,
 ACTIVO,
 MEDIDA 
}estado_automata;
estado_automata ESTADO;

void autom (void const *argument);                             // thread function
osThreadId id_autom;                                          // thread id
osThreadDef (autom, osPriorityNormal, 1, 0);                   // thread object

extern float lux;
int Init_automata (void) {
	
	GPIO_SetDir(PUERTO_RGB,RED,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_RGB,GREEN,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_RGB,BLUE,GPIO_DIR_OUTPUT);
	GPIO_PinWrite(PUERTO_RGB,RED,1);
	GPIO_PinWrite(PUERTO_RGB,GREEN,1);
	GPIO_PinWrite(PUERTO_RGB,BLUE,1);

  id_autom = osThreadCreate (osThread(autom), NULL);
  if (!id_autom) return(-1);
  
  return(0);
}

void autom (void const *argument) {
		init_lcd();
	LCD_reset();
  while (1) {
		
			copy_to_lcd();
			lcd_start();
			luxLCD(lux);
//    switch(ESTADO){
//			case INACTIVO: 
//				
//			break;
//			case ACTIVO:
//				
//			break;
//			
//			case MEDIDA:
//				
//			break;
//		}
		
		
    osThreadYield ();                                           // suspend thread
  }
}
