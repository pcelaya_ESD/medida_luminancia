#include "LPC17xx.h"												
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "Driver_SPI.h"
#include "SPI_LPC17xx.h"
//#include "Arial12x12.h"
#include "stdio.h"
#include "cmsis_os.h"       // CMSIS RTOS header file
#include "lcd.h"  
#include "Driver_I2C.h"

extern ARM_DRIVER_I2C            Driver_I2C2;
static ARM_DRIVER_I2C *I2Cdev = &Driver_I2C2;


#define ADDR 0x29
uint8_t ENA_REG =0xA0 ;
uint8_t ENA_CONFIG=0x13;
uint8_t CONTROL_REG =0xA1;
uint8_t CONTROL_CONFIG =0x12;
uint8_t CH0_L =0xB4;
uint8_t CH1_L =0xB6;

	//uint8_t buf[2]; // buffer donde se guardan los datos del sensor
	float cpl = 0;
  float lux=0, lux1 = 0;
	uint8_t dataCH0[1];
	uint8_t dataCH1[1];
	
void lum(void const *argument);
osThreadId id_lum;
osThreadDef (lum, osPriorityNormal,1,0);
	
int Init_I2C(void){
	
	id_lum = osThreadCreate(osThread(lum),NULL);
	if (!id_lum) return(-1);

  I2Cdev ->Initialize(NULL);
  I2Cdev ->PowerControl(ARM_POWER_FULL);
  I2Cdev ->Control(ARM_I2C_BUS_SPEED, ARM_I2C_BUS_SPEED_FAST);
  I2Cdev ->Control(ARM_I2C_BUS_CLEAR, 0);

		return(0);
}

void lum(void const *argument){

	//osDelay(1);
	
			//configuramos el ENABLE REGISTER
		I2Cdev->MasterTransmit (ADDR,&ENA_REG, 1, true);
		while (I2Cdev->GetStatus().busy){}
		I2Cdev ->MasterTransmit(ADDR,&ENA_CONFIG,1,true);
		while (I2Cdev->GetStatus().busy){	}
		
		//Configuramos el CONTROL REGISTER
		I2Cdev->MasterTransmit (ADDR,&CONTROL_REG, 1, true);
		while (I2Cdev->GetStatus().busy){}
		I2Cdev ->MasterTransmit(ADDR,&CONTROL_CONFIG,1,true);
		// ultimo parametro a false ya que no tenemos ninguna operacion pendiente
		while (I2Cdev->GetStatus().busy){	}

	while(1){

		
		//osDelay(300);
		
		//Leemos el canal 0 low
		I2Cdev->MasterTransmit (ADDR,&CH0_L, 1, true);
		while (I2Cdev->GetStatus().busy){}
		I2Cdev->MasterReceive(ADDR,dataCH0, 1, false); 
			while(I2Cdev->GetStatus().busy){}

		//Leemos el canal 1 low
		I2Cdev->MasterTransmit (ADDR,&CH1_L, 1, true);
		while (I2Cdev->GetStatus().busy){}
		I2Cdev->MasterReceive(ADDR,dataCH1, 1, false); 
			while(I2Cdev->GetStatus().busy){}
			
		//cpl = (ATIME * AGAIN)/CoeficienteLux
		cpl = (300*25)/762;
		lux1 = (dataCH0[0] - (2*dataCH1[0])/cpl);
		lux = (lux1/255)*100;

		}
}

float getLum(void){
	return lux;
}
