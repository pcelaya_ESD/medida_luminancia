#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "stdio.h"
#include "string.h"
#include "lcd.h"
#include "Driver_SPI.h"
#include "SPI_LPC17xx.h"
#include "joystick.h"

void debounceR (void const *argument);                             
osThreadId id_debounceR;                                          
osThreadDef (debounceR, osPriorityNormal, 1, 0);                   

void debounceF (void const *argument);                             
osThreadId id_debounceF;                                         
osThreadDef (debounceF, osPriorityNormal, 1, 0);

	int cont;
int Init_joystick (void) {
	
	GPIO_SetDir(PUERTO_JOYSTICK,sw_up,GPIO_DIR_INPUT);
	GPIO_SetDir(PUERTO_JOYSTICK,sw_down,GPIO_DIR_INPUT);
	GPIO_SetDir(PUERTO_JOYSTICK,sw_right,GPIO_DIR_INPUT);
	GPIO_SetDir(PUERTO_JOYSTICK,sw_left,GPIO_DIR_INPUT);
	GPIO_SetDir(PUERTO_JOYSTICK,sw_center,GPIO_DIR_INPUT);
	
	PIN_Configure(PUERTO_JOYSTICK,sw_up,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL); 
	PIN_Configure(PUERTO_JOYSTICK,sw_down,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JOYSTICK,sw_right,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JOYSTICK,sw_left,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JOYSTICK,sw_center,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
	LPC_GPIOINT->IO0IntEnR=((1<<sw_up)|(1<<sw_down)|(1<<sw_right)|(1<<sw_left)|(1<<sw_center));
	LPC_GPIOINT ->IO0IntClr =0xffffffff;  
	
  NVIC_ClearPendingIRQ(EINT3_IRQn);     
	NVIC_EnableIRQ(EINT3_IRQn);            

	id_debounceR = osThreadCreate (osThread(debounceR), NULL);
  id_debounceF = osThreadCreate (osThread(debounceF), NULL);
	
  if (!id_debounceR	| !id_debounceF) return(-1);
  return(0);
}

void EINT3_IRQHandler (void){
		if (LPC_GPIOINT->IO0IntStatR){osSignalSet(id_debounceR,01);}
	  if (LPC_GPIOINT->IO0IntStatF){osSignalSet(id_debounceF,01);}
		
	if (LPC_GPIOINT->IO0IntStatR  & 1 << sw_up){
			arriba = true;
			LPC_GPIOINT->IO0IntClr = 1 << sw_up;      
   }
	 if (LPC_GPIOINT->IO0IntStatR  & 1 << sw_down){
			abajo = true;
			LPC_GPIOINT->IO0IntClr = 1 << sw_down;      
   }
	 if (LPC_GPIOINT->IO0IntStatR  & 1 << sw_left){
			izquierda = true;
			LPC_GPIOINT->IO0IntClr = 1 << sw_left;     
	 }
	 if (LPC_GPIOINT->IO0IntStatR  & 1 << sw_center){
		 centro = true;
		 cont++;
			LPC_GPIOINT->IO0IntClr = 1 << sw_center;     
	 }
	  if (LPC_GPIOINT->IO0IntStatR  & 1 << sw_right){
			derecha = true;
			LPC_GPIOINT->IO0IntClr = 1 << sw_right;     
	 }
	 LPC_GPIOINT -> IO0IntEnR = 0 << sw_up | 0 << sw_down | 0 << sw_left | 0 << sw_right | 0 << sw_center; 
   LPC_GPIOINT -> IO0IntEnF = 0 << sw_up | 0 << sw_down | 0 << sw_left | 0 << sw_right | 0 << sw_center; 	 
	 LPC_GPIOINT->IO0IntClr = 0xFFFFFFFF;
} 

void debounceR (void const *argument) {

  while (1) {
    osSignalWait(01,osWaitForever);
    osDelay(200);
    LPC_GPIOINT -> IO0IntEnF = 1 << sw_up | 1 << sw_down | 1 << sw_left | 1 << sw_right | 1 << sw_center; 
    LPC_GPIOINT->IO0IntClr;
    osThreadYield ();                                           
  }
}

void debounceF (void const *argument) {

  while (1) {
    osSignalWait(01,osWaitForever);
    osDelay(200);
    LPC_GPIOINT -> IO0IntEnR = 1 << sw_up | 1 << sw_down | 1 << sw_left | 1 << sw_right | 1 << sw_center; 
    LPC_GPIOINT->IO0IntClr;
    osThreadYield ();                                           
  }
}
