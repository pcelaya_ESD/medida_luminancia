#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "Driver_USART.h"
#include <stdio.h>
#include <string.h>
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"

#define PUERTO_UART0 0
#define Tx0          2
#define Rx0          3

#define PUESTA_HORA 0x20
#define W_VALOR_CUENTA_A 0x25
#define R_VALOR_CUENTA_A 0x35
#define CICLO_MEDIDAS 0x70
#define N_MEDIDAS 0x40
#define ULT_MEDIDA 0x50
#define TODAS_MEDIDAS 0x55
#define BORRAR_MEDIDAS 0x60

void teraterm(void const *argument);
osThreadId id_tera;
osThreadDef (teraterm, osPriorityNormal,1,0);

/* USART Driver */
extern ARM_DRIVER_USART Driver_USART0;
static ARM_DRIVER_USART * USARTdrv = &Driver_USART0;
		
void myUSART_callback(uint32_t event){
switch (event){
		case ARM_USART_EVENT_RECEIVE_COMPLETE: //Recepci�n completada,para la sd
		//	osSignalSet(idThreadRX,rx_complete);
			break;
		case ARM_USART_EVENT_TRANSFER_COMPLETE: //Transferencia completada
		//	osSignalSet(idThreadRX,tx_complete);
			break;
		case ARM_USART_EVENT_SEND_COMPLETE: //Env�o completado
			
			break;
		case ARM_USART_EVENT_TX_COMPLETE: //Transmisi�n completada (opcional)
			
      break;
		}	
}
 
int UART_init(void){
    /*Inicializamos el USART driver */
    USARTdrv->Initialize(myUSART_callback);
    /*Power up the USART peripheral */
    USARTdrv->PowerControl(ARM_POWER_FULL);
    /*Configuramos el USART a 9600 Bits/sec */
    USARTdrv->Control(ARM_USART_MODE_ASYNCHRONOUS |
                      ARM_USART_DATA_BITS_8 |
                      ARM_USART_PARITY_NONE |
                      ARM_USART_STOP_BITS_1 |
                      ARM_USART_FLOW_CONTROL_NONE, 19200);
     
    /*Habilitamos las lineas de transmision y recepcion*/
    USARTdrv->Control (ARM_USART_CONTROL_TX, 1);
    USARTdrv->Control (ARM_USART_CONTROL_RX, 1);
	
	id_tera = osThreadCreate(osThread(teraterm),NULL);
	if (!id_tera) return(-1);
	
	return(0);
}

void teraterm(void const *argument){
	uint8_t  cmd[12];
	
	while(1){
		USARTdrv->Receive(cmd,12);
		
		switch(cmd[1]){
			case PUESTA_HORA:
				
			break;
			
			case W_VALOR_CUENTA_A:
				
			break;
			
			case R_VALOR_CUENTA_A:

				break;
			case CICLO_MEDIDAS:
				
			break;
			
			case N_MEDIDAS:
				
			break;
			
			case ULT_MEDIDA:
				
			break;
			
			case TODAS_MEDIDAS:
				
			break;
			
			case BORRAR_MEDIDAS:
				
			break;
			
		}
		osThreadYield ();    
	}
}


